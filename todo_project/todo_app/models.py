from django.contrib.auth.models import User
from django.db import models


class Todo(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.CharField(max_length=250)
    status = models.BooleanField(default=False)

    @classmethod
    def create(cls, user, text):
        todo = cls()
        todo.user = user
        todo.text = text
        todo.save() 
     
    
    def toggle_status(self):
        self.status = self.status
        self.save()


    def __str__(self):
        return f"{self.text}  {self.status}"
