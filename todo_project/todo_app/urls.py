from django.contrib import admin
from django.urls import path

from . import views


app_name = "todo_app"

urlpatterns = [
   path('', views.index, name='index'),
   path('completed_todos/', views.completed_todos, name='completed_todos'),
   path('update_status/', views.update_status, name='update_status'),
   path('delete_todo/', views.delete_todo, name='delete_todo'),
   
]
