from django.shortcuts import render, get_object_or_404, reverse
from .models import Todo
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

@login_required
def index(request):
    if request.method == 'POST':
        Todo.create(request.user, request.POST['text'])

    todos = Todo.objects.filter(status=False).filter(user=request.user)
    context = {
        'todos': todos,
    }
    return  render(request, 'todo_app/index.html', context)

@login_required
def completed_todos(request):
    todos = Todo.objects.filter(status=True).filter(user=request.user)
    context = {
            'todos': todos,
    }
    return render(request, 'todo_app/completed_todos.html', context)

@login_required
def update_status(request):
    pk = request.POST['pk']
    todo = get_object_or_404(Todo, pk=pk)
    todo.toggle_status()
    return HttpResponseRedirect(request.META['HTTP_REFERER'])

@login_required
def delete_todo(request):
    pk = request.POST['pk']
    todo = get_object_or_404(Todo, pk=pk)
    todo.delete()
    return HttpResponseRedirect(request.META['HTTP_REFERER'])
